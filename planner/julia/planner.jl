#!/usr/bin/env julia

# #=
# planner.jl:
# - Julia version: 1.6.2
# - Author: Andrej Kitanov
# - Date: 2021-07-24
# =#
#

using RobotOS

@rosimport std_msgs.msg: Header, Int32
@rosimport geometry_msgs.msg: PoseStamped
#@rosimport nav_msgs.msg: Path
#@rosimport visualization_msgs.msg: Marker
@rosimport std_msgs.msg: Float64, String, Int32
# Type: mrbsp_msgs/GenerateActions
@rosimport mrbsp_msgs.srv: GenerateActions

# by default rostypegen creates modules in Main
rostypegen()

using .std_msgs.msg: Int32
using .geometry_msgs.msg: Pose
using .mrbsp_msgs.srv: GenerateActions
#using .nav_msgs.msg: Path

function planning_session()

    planreq = mrbsp_msgs.srv.GenerateActionsRequest()
    planresp = mrbsp_msgs.srv.GenerateActionsResponse()
    # Args: start goal gtsam_values_str gtsam_factors_str
    planreq.start.position =  geometry_msgs.msg.Point(-0.15,-6.86,0.0)
    planreq.start.orientation =  geometry_msgs.msg.Quaternion(0.0,0.0,0.0,1.0)
    planreq.goal.position =  geometry_msgs.msg.Point(2.85,-6.86,0.0)
    planreq.goal.orientation =  geometry_msgs.msg.Quaternion(0.0,0.0,0.0,1.0)

    # set up service
    srvcall = ServiceProxy("generate_actions", GenerateActions)
    println("Waiting for 'generate_actions' service...")
    wait_for_service("generate_actions")

    planresp = srvcall(planreq)


    for i in 1:length(planresp.actions.actions)
        println("Action ", i)
        for j in 1:length(planresp.actions.actions[i].poses)
            x = planresp.actions.actions[i].poses[j]
            println("x", j, " = ", x)
        end
    end


    #path = nav_msgs.msg.Path()
    #push!(path.poses, posestamp)

    result = std_msgs.msg.Int32Msg(0)

    return result

end


function main()
    init_node("planner_julia")
    pub = Publisher{Int32}("opt_action", queue_size=1)

    idx = planning_session()

    publish(pub, idx)

    spin()
end

if ! isinteractive()
    main()
end
