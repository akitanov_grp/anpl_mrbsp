function R = cholesky(M)
% Input: M - symmetric positive semidefinite matrix
% Output: R - lower triangular sparse matrix that is Cholesky factor of M

[m, n] = size(M);
R = sparse([],[],[], n, n); % Initialize to all zeros
row = 1; col = 1;

for i = 1:n
    m11 = sqrt(M(1,1));
    R(row, col) = m11;
    if(m ~= 1)
        R21 = M(2:m,1)/m11;
        R(row+1:end, col) = R21;
        M = M(2:m, 2:m)-R21*R21';
        m = m-1;
        row = row+1;
        col = col+1;
    end
end
